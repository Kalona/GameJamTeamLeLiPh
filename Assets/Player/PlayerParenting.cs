﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParenting : MonoBehaviour
{
    PlayerController myPlayer;

    //bool doNotAtach = false;

    private void Start()
    {
        myPlayer.GetComponent<PlayerController>();
    }

    /*private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            DetachFromParent();
        }
    }*/

    void OnCollisionEnter(Collision other)
    {
        if (other.collider.gameObject.tag == "Ground")
        {
            //GameObject emptyObject = new GameObject();
            //emptyObject.transform.SetParent(other.collider.gameObject.transform);
            transform.SetParent(other.collider.gameObject.transform);
        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.collider.gameObject.tag == "Ground")
        {
            transform.parent = null;
        }
    }

    IEnumerator DetachFromParent()
    {
        //doNotAtach = true;

        yield return new WaitForSeconds(4);

        //doNotAtach = false;
    }
}
