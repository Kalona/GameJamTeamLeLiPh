﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fog : MonoBehaviour
{

    [SerializeField] float Speed = 1;

    Quaternion Rotation;
     [SerializeField] GameObject Parent;

     bool GoUp = false;
    float OriginDistance = 0;

    void Awake()
    {
        Rotation = transform.rotation;

          if(!Parent)
        Parent = transform.parent.gameObject;

         OriginDistance = Parent.transform.position.y - transform.position.y;
         Debug.Log("Origin"+OriginDistance);
    }

    void Update()
    {
            transform.position = new Vector3(
                 transform.position.x
                ,Mathf.MoveTowards(transform.position.y,  -OriginDistance, Speed * Time.deltaTime)
                ,  transform.position.z)       ;   
    }

    void LateUpdate()
    {
        transform.rotation = Rotation;
    }
}
