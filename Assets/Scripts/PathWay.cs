﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathWay : MonoBehaviour
{

    // PATH SETTINGS
    [Header("Path Properties:")]
    public List<GameObject> PathPoints = new List<GameObject>();
    [SerializeField] float waitTimePerDot = 0;
    [SerializeField] float Speed = 5;
    float Timer;

    bool GoBack = false;
    bool GoForward = true;

    bool GotoStart = false;
    protected int ZählerVariable = 0;

    [SerializeField] bool UnMoving = true;
    [SerializeField] bool Return = false;


    // Start is called before the first frame update
    void Start()
    {
        // Use the current player position as the first start point
        if (!UnMoving)
        {
            GameObject EmptyObject = new GameObject("StartPoint");
            EmptyObject.transform.position = transform.position;
            PathPoints.Insert(0, EmptyObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!UnMoving && Return)
            ReturnPlattform();
    }

    protected virtual void ReturnPlattform()
    {
        if (transform.position == PathPoints[ZählerVariable].transform.position)
        {
            Timer += Time.deltaTime;

            if (Timer > waitTimePerDot)
            {
                if (GoForward == true)
                {
                    ZählerVariable++;
                }
                if (GoBack == true)
                {
                    ZählerVariable--;
                }

                Timer = 0;
            }
        }

        transform.position = Vector3.MoveTowards(transform.position, PathPoints[ZählerVariable].transform.position, Speed * Time.deltaTime);
        if (transform.position == PathPoints[PathPoints.Count - 1].transform.position)
        {
            GoBack = true;
            GoForward = false;
        }
        if (transform.position == PathPoints[0].transform.position)
        {
            GoBack = false;
            GoForward = true;
        }
    }
}
