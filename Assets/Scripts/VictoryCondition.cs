﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryCondition : MonoBehaviour
{
    [SerializeField] bool victory = false;

    [SerializeField] GameObject VictoryText;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            victory = true;
        }
    }

    private void Update()
    {
        if (victory)
        {
            VictoryText.SetActive(true);
            //Destroy(gameObject);
        }
    }
}
