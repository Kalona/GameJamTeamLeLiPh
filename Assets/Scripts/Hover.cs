﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour
{
    Animator myAnimator;


    // Start is called before the first frame update
    void Start()
    {
        myAnimator = GetComponent<Animator>();

        StartCoroutine(WaitForInitiation());
    }

    IEnumerator WaitForInitiation()
    {
        yield return new WaitForSeconds(Random.Range(0f, 5f));

        myAnimator.SetBool("Initiated", true);
    }
}
