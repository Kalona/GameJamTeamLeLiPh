﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
       static AudioSource audioSrc;
    public static AudioClip Impact;
    bool StartAudio = false;

    // Start is called before the first frame update
    void Awake()
    {
Impact = Resources.Load<AudioClip>("Impact");
audioSrc = GetComponent<AudioSource>();
if(audioSrc == null) audioSrc = gameObject.AddComponent<AudioSource>();
    }

    public static void PlayAudio()
    {
audioSrc.PlayOneShot(Impact);
Debug.Log("DoIt");
    }
}
