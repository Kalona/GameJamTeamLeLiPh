﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController1 : MonoBehaviour
{
    Rigidbody MyRigidBody;

    // CAMRA
    [Header("Camera / Mouse Look Properties:")]
    [SerializeField] Vector2 MouseSensivity = new Vector2(10f, 10f);
    [SerializeField] float MouseRotationYMax = 90;
    [SerializeField] Camera MainCamera = new Camera();

    private float RotationX;
    private float RotationY;

    // FOG
    [Header("Fog Properties:")]
    [SerializeField] GameObject Object;

    // MOVE
    [Header("Move Properties:")]
    [Range(0, 1)]
    [SerializeField] float MoveSpeed;

    // JUMP
    [Header("Jump Properties:")]
    [SerializeField] KeyCode Jump = new KeyCode();
    [SerializeField] LayerMask WhatIsGround;
    [SerializeField] Transform FeetPos;
    [SerializeField] float RoundRadius = 1;
    [Range(0, 30)]
    [SerializeField] float JumpForce = 10;
    [Range(0, 30)]
    [SerializeField] float DoubleJumpForce = 10;
    [Range(0, 30)]
    [SerializeField] float DoubleJumpForwardForce = 10;

    bool DoubleJumpAllowance = false;
    bool IsGrounded;
    bool IsJumping = false;

    bool MakeJump;
    bool MakeDoubleJump;

   // JUMP
    [Header("Gravity Properties:")]
    
    [SerializeField] float FallGravity = -9.84f;
    [SerializeField] float JumpGravity = -9.84f;
     [SerializeField] KeyCode AirControlKey = new KeyCode();
    [SerializeField] float AirControlGravity = -9.84f;

 [Header("Menu Properties:")]
 [SerializeField] GameObject Menu;
    [SerializeField] KeyCode MenuKey = new KeyCode();


    // Start is called before the first frame update
    void Start()
    {
        // Feet
        MyRigidBody = GetComponent<Rigidbody>();
        if (!MainCamera)
            MainCamera = FindObjectOfType<Camera>();
        FeetPos = GetComponent<Transform>();

        // 

        // Lock cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

    }

    // Update is called once per frame
    void Update()
    {
        // Player rotation
        RotationX += MouseSensivity.x * Input.GetAxis("Mouse X");
        transform.eulerAngles = new Vector3(0, RotationX, 0);

        // Mouse rotation
        RotationY -= MouseSensivity.y * Input.GetAxis("Mouse Y");
        RotationY = Mathf.Clamp(RotationY, -MouseRotationYMax, MouseRotationYMax);
        MainCamera.transform.eulerAngles = new Vector3(RotationY, RotationX, 0);

        // jumping
        IsGrounded = Physics.CheckSphere(FeetPos.position, RoundRadius, WhatIsGround);

        if (IsGrounded)
            MyRigidBody.drag = 10;
        else
            MyRigidBody.drag = 0;

        if (Input.GetKeyDown(Jump))
        {
            IsJumping = true;
        }
        else
        {
            IsJumping = false;
        }

        if (IsJumping)
        {
            AudioManager.PlayAudio();
            if (IsGrounded)
            {
                MakeJump = true;
                DoubleJumpAllowance = true;
            }
            else
            {
                if (DoubleJumpAllowance)
                {

                    DoubleJumpAllowance = false;
                    MakeDoubleJump = true;
                }
            }
        }



         // Gravity Settings
        if(Input.GetKey(AirControlKey))
        {
         Physics.gravity = new Vector3(0,AirControlGravity,0);
        }
         else
         {
                     if(MakeJump || MakeDoubleJump)
                      Physics.gravity = new Vector3(0,JumpGravity,0);
             else if(!IsGrounded)
               Physics.gravity = new Vector3(0,FallGravity,0);
       
         }

          if(Input.GetKeyDown(MenuKey))
        {
if(Menu.activeSelf)
Menu.SetActive(false);
else
Menu.SetActive(true);
{
    
}

        }

    }

    void FixedUpdate()
    {
        // define moving direction
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);

        // moving
        Vector3 move = (right * Input.GetAxis("Horizontal") * MoveSpeed) + (forward * Input.GetAxis("Vertical") * MoveSpeed);
        MyRigidBody.MovePosition(transform.position + new Vector3(move.x, 0, move.z));

        // jumping
        if (MakeJump)
        {
            MyRigidBody.AddForce(transform.up * JumpForce, ForceMode.Impulse);
            MyRigidBody.velocity = new Vector3(MyRigidBody.velocity.x, 0, MyRigidBody.velocity.z);

            DoubleJumpAllowance = true;
            MakeJump = false;
        }

        // doubleJump
        if (MakeDoubleJump)
        {
            MyRigidBody.AddForce(transform.up * DoubleJumpForce, ForceMode.Impulse);
            MyRigidBody.AddForce(transform.forward * DoubleJumpForwardForce, ForceMode.Impulse);
            MyRigidBody.velocity = new Vector3(MyRigidBody.velocity.x, 0, 0);

            DoubleJumpAllowance = false;
            MakeDoubleJump = false;
        }
    }
}
