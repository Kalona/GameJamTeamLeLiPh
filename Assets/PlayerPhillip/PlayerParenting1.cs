﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParenting1 : MonoBehaviour
{
    void OnCollisionEnter(Collision collider)
    {
        if(collider.gameObject.tag == "Ground")
        {
            transform.parent = collider.gameObject.transform;
        }
    }

    void OnCollisionExit(Collision collider)
    {
        DetachFromParent();
    }

    void DetachFromParent()
    {
        transform.parent = null;
    }
}
